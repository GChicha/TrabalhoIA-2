from jogo import Jogo, Sensor, Coordenada, Agente
from itertools import product
from tkinter import Canvas, Tk

root = Tk()
jogo = Jogo(10,10,2)
jogador = Agente()

canvas = Canvas(root,width=700, height=700)
def getImage(pos, kB):
    canvas.pack()

    xInicial = pos.x - 3
    yInicial = pos.y - 3

    xFinal = pos.x + 3
    yFinal = pos.y + 3

    xRange = range(xInicial, xFinal + 1)
    yRange = range(yInicial, yFinal + 1)

    posicao = product(range(0, 700, 100), range(0, 700, 100))
    coordenadas = product(xRange, yRange)

    coordenadas = zip(coordenadas, posicao)

    for coordenada in coordenadas:
        atual = Coordenada(coordenada[0][0], coordenada[0][1])
        knowledge = kB[atual]

        if knowledge.parede:
            cor = "brown"
        elif knowledge.wumpus:
            cor = "green"
        elif knowledge.buraco:
            cor = "blue"
        elif knowledge.ouro:
            cor = "yellow"
        elif knowledge.fedor is False and knowledge.brisa is False:
            if atual == pos:
                cor = "white"
            else:
                cor = "gray"
        else:
            cor = "black"

        canvas.create_rectangle(coordenada[1][0], coordenada[1][1], coordenada[1][0] + 100, coordenada[1][1] + 100, fill=cor)

def run(event):
    if jogo.gold == jogo.posJogador:
        exit()
    getImage(jogador.posRelativa, jogador.kB)
    jogo.moveJogador(jogador.nextStep(jogo.getSensor()))

root.bind("<Button-1>", run)
root.mainloop()
