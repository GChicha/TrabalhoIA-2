from collections import defaultdict, namedtuple
from functools import reduce
from random import choice, sample
from enum import IntEnum

Coordenada = namedtuple("Coordenada", ['x', 'y'])
Sensor = namedtuple("Sensor", ["fedor", "brilho", "brisa", "parede", "wumpus", "buraco", "ouro"])

Sensor.__new__.__defaults__ = (None, ) * len(Sensor._fields)

def posToCoord(pos, tamLinha):
    x = pos % tamLinha
    y = pos // tamLinha

    return Coordenada(x, y)

def getPosAdj(posicao):
    x, y = posicao

    return [Coordenada(x, y - 1),
            Coordenada(x, y + 1),
            Coordenada(x + 1, y),
            Coordenada(x - 1, y)]

class Movimentos(IntEnum):
    CIMA = 0
    BAIXO = ~CIMA
    DIREITA = 1
    ESQUERDA = ~DIREITA

    IMPASSE = 50

def newPos(oldPos, mov):
    if mov == Movimentos.CIMA:
        return Coordenada(oldPos.x, oldPos.y - 1)
    elif mov == Movimentos.BAIXO:
        return Coordenada(oldPos.x, oldPos.y + 1)
    elif mov == Movimentos.DIREITA:
        return Coordenada(oldPos.x + 1, oldPos.y)
    elif mov == Movimentos.ESQUERDA:
        return Coordenada(oldPos.x - 1, oldPos.y)

class Agente(object):
    def __init__(self):
        self.caminho = []

        self.posRelativa = Coordenada(0, 0)
        self.kB = defaultdict(Sensor)

        self.resolvido = set()

    def _update_kb(self, sensor):
        adjacentes = getPosAdj(self.posRelativa)

        for adjacente in filter(lambda x: x not in self.resolvido , adjacentes):
            fedor, brilho, brisa, parede, wumpus, buraco, ouro = self.kB[adjacente]

            if sensor.fedor:
                if fedor:
                    wumpus = True
                    self.resolvido.add(adjacente)
                elif fedor is None:
                    fedor = True
            else:
                fedor = False

            if sensor.brisa:
                if brisa:
                    buraco = True
                    self.resolvido.add(adjacente)
                elif brisa is None:
                    brisa = True
            else:
                brisa = False

            if sensor.brilho:
                if brilho:
                    ouro = True
                elif brilho is None:
                    brilho = True
            else:
                brilho = False

            self.kB[adjacente] = Sensor(fedor, brilho, brisa, parede, wumpus, buraco, ouro)

    def nextStep(self, sensor):
        self.resolvido.add(self.posRelativa)

        if not sensor.parede:
            self._update_kb(sensor)

            possiveis = zip(Movimentos, getPosAdj(self.posRelativa))

            possiveis = list(filter(lambda x: x[1] not in self.resolvido and not self.kB[x[1]].fedor and not self.kB[x[1]].brisa, possiveis))

            if len(possiveis) > 0:
                movimento = choice(possiveis)[0]
                self.caminho.append(movimento)
                self.posRelativa = newPos(self.posRelativa, movimento)
            else:
                if len(self.caminho) > 0:
                    movimento = ~self.caminho.pop()
                    self.posRelativa = newPos(self.posRelativa, movimento)
                else:
                    movimento = Movimentos.IMPASSE
        else:
            self.kB[self.posRelativa] = Sensor(parede = True)
            movimento = ~self.caminho.pop()
            self.posRelativa = newPos(self.posRelativa, movimento)

        return movimento

class Jogo(object):
    def __init__(self, n = 4, m = 4, holes=2):
        self.size_width = n
        self.size_height = m

        totalSalas = n * m

        posVetJogador = choice(range(totalSalas))
        usedPos = [posVetJogador]

        posVetWumpus = choice(list(filter(lambda x: x not in usedPos, range(totalSalas))))
        usedPos.append(posVetWumpus)

        posVetGold = choice(list(filter(lambda x: x not in usedPos,  range(totalSalas))))
        usedPos.append(posVetGold)

        posVetHoles = sample(list(filter(lambda x: x not in usedPos, range(totalSalas))), holes)
        usedPos += posVetHoles

        self.posJogador = posToCoord(posVetJogador, self.size_width)
        self.posWumpus = posToCoord(posVetWumpus, self.size_width)
        self.gold = posToCoord(posVetGold, self.size_width)
        self.holes = []

        for pos in posVetHoles:
            self.holes.append(posToCoord(pos, self.size_width))

    def getSensor(self): # Com base nas posições do ouro / wumpus / buracos retorna o sensor do jogador
        coordenadasAdjacentes = getPosAdj(self.posJogador)

        parede, wumpus, buraco, fedor, brisa, brilho, ouro = (None,) * 7

        if not (self.size_width > self.posJogador.x >= 0 and self.size_height > self.posJogador.y >= 0):
            parede = True
        else:
            ouro = True if self.posJogador == self.gold else False
            wumpus = True if self.posJogador == self.posWumpus else False
            buraco = True if self.posJogador in self.holes else False

            fedor = reduce(lambda x, y: x or self.posWumpus == y, [False] + coordenadasAdjacentes)
            brisa = reduce(lambda x, y: x or y in self.holes, [False] + coordenadasAdjacentes)
            brilho = reduce(lambda x, y: x or self.gold == y, [False] + coordenadasAdjacentes)

        return Sensor(parede = parede, ouro = ouro, wumpus = wumpus, buraco = buraco, fedor = fedor, brisa = brisa, brilho = brilho)

    def moveJogador(self, movimento):
        if movimento == Movimentos.IMPASSE:
            print("IMPASSE")
            pass
        else:
            self.posJogador = newPos(self.posJogador, movimento)
